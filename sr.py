from data_loader import DataLoader
import datetime
from keras.layers import Activation, Add, BatchNormalization, Input
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import Conv2D, UpSampling2D
from keras.models import Model
from keras.optimizers import Adam
import numpy as np
from options import SROptions
import os

import keras.backend as K

class SR:

    def __init__(self, options):
        # Input shape
        self.channels = 3
        self.lr_height = options.lr_height          # Low resolution height
        self.lr_width = options.lr_width            # Low resolution width
        self.lr_shape = (self.lr_height, self.lr_width, self.channels)
        self.hr_height = self.lr_height*4   # High resolution height
        self.hr_width = self.lr_width*4     # High resolution width
        self.hr_shape = (self.hr_height, self.hr_width, self.channels)
        self.options = options
        self.eps = 1e-6
        self.n_recursive_blocks = 8
        self.n_convolition_layers = 4

        optimizer = Adam(0.0002, 0.5)

        self.dataset_name = options.dataset_name
        self.data_loader = DataLoader(dataset_name=self.dataset_name,
                                      options=options)

        def L1_Charbonnier_loss(y_true, y_pred):
            return K.mean(K.sqrt(K.square(y_pred - y_true) + self.eps), axis=-1)

        self.network = self.build_network()
        self.network.compile(loss=L1_Charbonnier_loss,
                             optimizer=optimizer)


    def build_network(self):

        img_lr = Input(shape=self.lr_shape)

        c1 = Conv2D(64, kernel_size=9, strides=1, padding='same')(img_lr)
        c1 = Activation('relu')(c1)

        def recursive_block(layer_input, filters=64, strides=1, bn=True):
            d = LeakyReLU(alpha=0.2)(layer_input)
            d = Conv2D(filters, kernel_size=3, strides=strides, padding='same', use_bias=False)(d)
            output = Add()([d, layer_input])

            for _ in range(self.n_convolition_layers - 1):
                d = LeakyReLU(alpha=0.2)(d)
                d = Conv2D(filters, kernel_size=3, strides=strides, padding='same', use_bias=False)(d)
                output = Add()([d, output])

            return output

        def deconv2d(layer_input):
            """Layers used during upsampling"""
            u = UpSampling2D(size=2)(layer_input)
            u = Conv2D(256, kernel_size=3, strides=1, padding='same')(u)
            u = Activation('relu')(u)
            return u

        r = recursive_block(c1)
        for _ in range(self.n_recursive_blocks - 1):
            r = recursive_block(r)

        c2 = Conv2D(64, kernel_size=3, strides=1, padding='same')(r)
        c2 = BatchNormalization(momentum=0.8)(c2)
        c2 = Add()([c2, c1])

        # Upsampling
        u1 = deconv2d(c2)
        u2 = deconv2d(u1)

        gen_hr = Conv2D(self.channels, kernel_size=9, strides=1, padding='same', activation='tanh')(u2)

        return Model(img_lr, gen_hr)

    def save_model(self):

        def save(model, model_name, save_name):
            if not os.path.exists('saved_model/' + save_name):
                os.makedirs('saved_model/' + save_name)
            model_path = "saved_model/{}/{}.json".format(save_name, model_name)
            weights_path = "saved_model/{}/{}_weights.hdf5".format(save_name, model_name)
            options = {"file_arch": model_path, 
                        "file_weight": weights_path}
            json_string = model.to_json()
            open(options['file_arch'], 'w').write(json_string)
            model.save_weights(options['file_weight'])

        save(self.network, "SR", self.options.save_name)

    def train(self, epochs, batch_size=1):

        start_time = datetime.datetime.now()

        for epoch in range(epochs):

            # Sample images and their conditioning counterparts
            imgs_hr, imgs_lr = self.data_loader.load_data(batch_size)

            loss = self.network.train_on_batch(imgs_lr, imgs_hr)

            elapsed_time = datetime.datetime.now() - start_time
            # Plot the progress
            print ("%d time: %s" % (epoch, elapsed_time))

    def load_model(self):

        def load(model, model_name, save_name):
            weights_path = "saved_model/{}/{}_weights.hdf5".format(save_name, model_name)
            model.load_weights(weights_path)

        load(self.network, "SR", self.options.save_name)

if __name__ == '__main__':
    options = SROptions()
    sr = SR(options)
    train_y, train_x = sr.data_loader.load_all_data()
    test_y, test_x = sr.data_loader.load_all_data(True)
    try:
        history = sr.network.fit(x=[train_x], y=[train_y], batch_size=options.batch_size, 
                                 epochs=options.epochs, verbose=1, validation_data=([test_x], [test_y]))
    finally:
        sr.save_model()