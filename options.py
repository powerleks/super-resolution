class Options:
    def __init__(self):
        self.path_hr = '/DIV2K_train_HR_cropped/{}.png'
        self.path_lr = 'DIV2K_train_LR_bicubic_cropped/{}x4.png'
        self.valid_hr = '/DIV2K_valid_HR_cropped/{}.png'
        self.valid_lr = '/DIV2K_valid_LR_bicubic_cropped/{}x4.png'
        self.dataset_name = 'div2k'
        self.lr_height = 96
        self.lr_width = 96

    def get_options(self):
        return None

    def save(self, additional_options=None):
        options = self.get_options()
        if not options:
            return
        text = ""
        for key, value in options.items():
            text += "{}: {}\n".format(key, str(value))
        if additional_options:
            for key, value in additional_options.items():
                text += "{}: {}\n".format(key, str(value))
        path = 'saved_model/{}/'.format(options['save_name']) if 'save_name' in options else ''
        with open(path + 'report.txt', 'w') as f:
            f.write(text)

class SROptions(Options):
    def __init__(self, loss=None, loss_weights=None):
        super().__init__()
        self.name = 'sr'
        self.epochs = 300
        self.batch_size = 10
        self.save_name = 'standard'

    def get_options(self):
        info = dict(name=self.name,
                    epochs=self.epochs,
                    batch_size=self.batch_size,
                    save_name=self.save_name)
        return info
