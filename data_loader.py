import scipy
import numpy as np


class DataLoader:
    def __init__(self, dataset_name, options):
        self.dataset_name = dataset_name
        self.options = options

    def load_data(self, batch_size=1, is_testing=False):
        data_type = "train" if not is_testing else "test"

        batch_images = np.random.choice(np.arange(1, 1601, 1), size=batch_size)

        imgs_hr = []
        imgs_lr = []
        for image in batch_images:
            img_path = self.options.path_hr.format(str(image).zfill(4))
            img_hr = self.imread(img_path)
            img_path = self.options.path_lr.format(str(image).zfill(4))
            img_lr = self.imread(img_path)

            # If training => do random flip
            if not is_testing and np.random.random() < 0.5:
                img_hr = np.fliplr(img_hr)
                img_lr = np.fliplr(img_lr)

            imgs_hr.append(img_hr)
            imgs_lr.append(img_lr)

        imgs_hr = np.array(imgs_hr) / 127.5 - 1.
        imgs_lr = np.array(imgs_lr) / 127.5 - 1.

        return imgs_hr, imgs_lr

    def load_all_data(self, is_validating=False):

        batch_images = np.arange(1, 11, 1) if not is_validating else np.arange(801, 811, 1)

        imgs_hr = []
        imgs_lr = []
        for image in batch_images:
            path = self.options.path_hr if not is_validating else self.options.valid_hr
            img_path = path.format(str(image).zfill(4))
            img_hr = self.imread(img_path)
            path = self.options.path_lr if not is_validating else self.options.valid_lr
            img_path = path.format(str(image).zfill(4))
            img_lr = self.imread(img_path)

            imgs_hr.append(img_hr)
            imgs_lr.append(img_lr)

        imgs_hr = np.array(imgs_hr) / 127.5 - 1.
        imgs_lr = np.array(imgs_lr) / 127.5 - 1.
        # imgs_lr = np.array(imgs_lr) / 255

        return imgs_hr, imgs_lr

    def load_img(self, path):
        imgs = [self.imread(path)]
        imgs = np.array(imgs) / 127.5 - 1.
        return imgs


    def imread(self, path):
        return scipy.misc.imread(path, mode='RGB').astype(np.float)

